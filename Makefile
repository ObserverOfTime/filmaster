all: generate train

## generate the dataset
generate: templates/*.chatl
	@pychatl $^ -a snips >filmaster/dataset.json

## train the NLU engine
train: filmaster/dataset.json
	@rm -rf nlu_engine
	@snips-nlu train -v $< nlu_engine

## run the Discord bot
run: TMDB_API_KEY ?= $(error missing TMDB_API_KEY)
run: DISCORD_TOKEN ?= $(error missing DISCORD_TOKEN)
run: V = $(if $(DEBUG),-$(shell printf 'v%.0s' {1..$(DEBUG)}),)
run: ; @python3 -m filmaster $(V) -t $(TMDB_API_KEY) -d $(DISCORD_TOKEN)

.PHONY: docs

## lint the package
lint:
	@flake8 --show-source filmaster
	@isort -rc -c -df filmaster

## generate modules documentation
docs:
	@sphinx-build -M html docs docs/_build

## show this help message
help:
	@gawk '/^##/{help=substr($$0, 3); getline; \
		print $$1help}' $(lastword $(MAKEFILE_LIST))
