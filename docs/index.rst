filmaster modules
=================

.. automodule:: filmaster
   :members:
   :show-inheritance:

filmaster.bot module
--------------------

.. automodule:: filmaster.bot
   :members:
   :show-inheritance:

filmaster.nlu module
--------------------

.. automodule:: filmaster.nlu
   :members:
   :show-inheritance:

filmaster.tmdb module
---------------------

.. automodule:: filmaster.tmdb
   :members:
   :show-inheritance:
