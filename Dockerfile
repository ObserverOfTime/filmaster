FROM python:3.7-slim-buster

MAINTAINER ObserverOfTime <chronobserver@disroot.org>

WORKDIR /app

COPY pyproject.toml .
COPY poetry.lock .

RUN pip install --no-cache-dir 'poetry>=1.0'
RUN poetry config virtualenvs.create false
RUN poetry export --without-hashes \
      -f requirements.txt -o /requirements.txt
RUN pip install --no-cache-dir -r /requirements.txt

COPY filmaster filmaster
COPY nlu_engine nlu_engine

ENTRYPOINT ["python3", "-mfilmaster"]
CMD ["--help"]
