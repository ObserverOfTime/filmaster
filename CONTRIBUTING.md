# Contibuting

### Setup

Install [Poetry](https://github.com/sdispater/poetry).

```sh
$ curl -LSs https://git.io/get-poetry | python
$ # or with pip
$ pip install --user 'poetry>=1.0'
```

Clone the repository.

```sh
$ git clone https://gitlab.com/ObserverOfTime/filmaster
$ cd filmaster
```

Install the dependencies.

```sh
$ poetry install
$ poetry run snips-nlu download en
```

### Modules

Module documentation is available [here][gl-page].

[gl-page]: https://observeroftime.gitlab.io/filmaster/

### Tests

Lint the package:

```sh
$ poetry run make lint
```

*Unit tests may be added at a later time*.

### Templates

Templates are written in [chatl][] format.

After editing them, don't forget to
generate the dataset & retrain the engine:

```sh
$ poetry run make generate train
```

[chatl]: https://github.com/atlassistant/chatl
